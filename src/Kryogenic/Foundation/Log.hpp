#ifndef KRYOGENIC_FOUNDATION_LOG_HPP
#define KRYOGENIC_FOUNDATION_LOG_HPP

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "Kryogenic/Foundation/Types.hpp"

namespace Kryogenic {
	struct Log final {
		static inline const auto Logger = spdlog::stdout_color_mt("kryogenic");

		constexpr Log() noexcept  = delete;
		constexpr ~Log() noexcept = delete;

		Log(Log const& pOther)                        = delete;
		Log(Log&& pOther) noexcept                    = delete;
		auto operator=(Log const& pOther) -> Log&     = delete;
		auto operator=(Log&& pOther) noexcept -> Log& = delete;

		auto static SetLevel(spdlog::level::level_enum const pLevel) -> void {
			Logger->set_level(pLevel);
		}

		auto static Flush() -> void {
			Logger->flush();
		}

		auto static Debug(cstring pMessage) -> void {
			Logger->debug(pMessage);
		}

		auto static Trace(cstring pMessage) -> void {
			Logger->trace(pMessage);
		}

		auto static Info(cstring pMessage) -> void {
			Logger->info(pMessage);
		}

		auto static Warn(cstring pMessage) -> void {
			Logger->warn(pMessage);
		}

		auto static Error(cstring pMessage) -> void {
			Logger->error(pMessage);
		}

		auto static Critical(cstring pMessage) -> void {
			Logger->critical(pMessage);
		}

		template<typename... ARGS, typename = std::enable_if_t<sizeof...(ARGS) != 0>>
		auto static Debug(spdlog::format_string_t<ARGS...> pFormat, ARGS... pArgs) -> void {
			Logger->debug(pFormat, std::forward<ARGS>(pArgs)...);
		}

		template<typename... ARGS, typename = std::enable_if_t<sizeof...(ARGS) != 0>>
		auto static Trace(spdlog::format_string_t<ARGS...> pFormat, ARGS... pArgs) -> void {
			Logger->trace(pFormat, std::forward<ARGS>(pArgs)...);
		}

		template<typename... ARGS, typename = std::enable_if_t<sizeof...(ARGS) != 0>>
		auto static Info(spdlog::format_string_t<ARGS...> pFormat, ARGS... pArgs) -> void {
			Logger->info(pFormat, std::forward<ARGS>(pArgs)...);
		}

		template<typename... ARGS, typename = std::enable_if_t<sizeof...(ARGS) != 0>>
		auto static Warn(spdlog::format_string_t<ARGS...> pFormat, ARGS... pArgs) -> void {
			Logger->warn(pFormat, std::forward<ARGS>(pArgs)...);
		}

		template<typename... ARGS, typename = std::enable_if_t<sizeof...(ARGS) != 0>>
		auto static Error(spdlog::format_string_t<ARGS...> pFormat, ARGS... pArgs) -> void {
			Logger->error(pFormat, std::forward<ARGS>(pArgs)...);
		}

		template<typename... ARGS, typename = std::enable_if_t<sizeof...(ARGS) != 0>>
		auto static Critical(spdlog::format_string_t<ARGS...> pFormat, ARGS... pArgs) -> void {
			Logger->critical(pFormat, std::forward<ARGS>(pArgs)...);
		}
	};
} // Kryogenic

#endif //KRYOGENIC_FOUNDATION_LOG_HPP
