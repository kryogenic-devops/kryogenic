#ifndef KRYOGENIC_FOUNDATION_ENSURE_HPP
#define KRYOGENIC_FOUNDATION_ENSURE_HPP

#include <cstdlib>

#include "Kryogenic/Foundation/Log.hpp"
#include "Kryogenic/Foundation/Types.hpp"

namespace Kryogenic {
	struct Ensure final {
		constexpr Ensure() noexcept  = delete;
		constexpr ~Ensure() noexcept = delete;

		Ensure(Ensure const& pOther)                        = delete;
		Ensure(Ensure&& pOther) noexcept                    = delete;
		auto operator=(Ensure const& pOther) -> Ensure&     = delete;
		auto operator=(Ensure&& pOther) noexcept -> Ensure& = delete;

		constexpr static auto True(b8 const pCondition, cstring pMessage) -> void {
			if (false == pCondition) {
				Log::Critical(pMessage);
				std::abort();
			}
		}

		constexpr static auto False(b8 const pCondition, cstring pMessage) -> void {
			if (true == pCondition) {
				Log::Critical(pMessage);
				std::abort();
			}
		}

		constexpr static auto Null(void const* const pPointer, cstring pMessage) -> void {
			if (nullptr != pPointer) {
				Log::Critical(pMessage);
				std::abort();
			}
		}

		constexpr static auto NotNull(void const* const pPointer, cstring pMessage) -> void {
			if (nullptr == pPointer) {
				Log::Critical(pMessage);
				std::abort();
			}
		}
	};
} // Kryogenic

#endif //KRYOGENIC_FOUNDATION_ENSURE_HPP
