#ifndef KRYOGENIC_CORE_APP_CTX_HPP
#define KRYOGENIC_CORE_APP_CTX_HPP

#include "Kryogenic/Core/WndCtx.hpp"
#include "Kryogenic/Foundation/Types.hpp"

namespace Kryogenic {
	class WndCtx;
} // Kryogenic

namespace Kryogenic {
	class AppCtx {
	public:
		AppCtx() noexcept;
		~AppCtx() noexcept = default;

		AppCtx(AppCtx const& pOther)                        = delete;
		AppCtx(AppCtx&& pOther) noexcept                    = delete;
		auto operator=(AppCtx const& pOther) -> AppCtx&     = delete;
		auto operator=(AppCtx&& pOther) noexcept -> AppCtx& = delete;

		static auto Instance() noexcept -> AppCtx&;

		auto Execute() noexcept -> void;
		auto Terminate() noexcept -> void;

	private:
		static inline AppCtx* sInstance = nullptr;

		b8 mIsRunning = {};

		std::unique_ptr<WndCtx> mWndCtx = {};
	};
} // Kryogenic

#endif //KRYOGENIC_CORE_APP_CTX_HPP
