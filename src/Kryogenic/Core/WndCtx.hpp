#ifndef KRYOGENIC_CORE_WND_CTX_HPP
#define KRYOGENIC_CORE_WND_CTX_HPP

#include "Kryogenic/Foundation/Types.hpp"

namespace Kryogenic {
	class WndCtx {
	public:
		struct Desc {
			cstring mTitle  = "Kryogenic";
			u16     mWidth  = 800;
			u16     mHeight = 600;
		};

		WndCtx() noexcept = delete;
		~WndCtx() noexcept;

		explicit WndCtx(Desc const& pDesc);

		WndCtx(WndCtx const& pOther)                        = delete;
		WndCtx(WndCtx&& pOther) noexcept                    = delete;
		auto operator=(WndCtx const& pOther) -> WndCtx&     = delete;
		auto operator=(WndCtx&& pOther) noexcept -> WndCtx& = delete;

		auto Update() noexcept -> b8;

		template<class T>
		auto As() noexcept -> T* {
			return static_cast<T*>(mHandle);
		}

	private:
		void*   mHandle = {};
		cstring mTitle  = {};
		u16     mWidth  = {};
		u16     mHeight = {};

		b8 mIsOpen       = {};
		b8 mIsMinimized  = {};
		b8 mIsMaximized  = {};
		b8 mIsResized    = {};
		b8 mIsFocused    = {};
		b8 mIsFullscreen = {};
	};
} // Kryogenic

#endif //KRYOGENIC_CORE_WND_CTX_HPP
