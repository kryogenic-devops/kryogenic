#include "WndCtx.hpp"

#include <SDL2/SDL.h>

#include "Kryogenic/Foundation/Ensure.hpp"

namespace Kryogenic {
	WndCtx::WndCtx(Desc const& pDesc)
		: mTitle(pDesc.mTitle)
		, mWidth(pDesc.mWidth)
		, mHeight(pDesc.mHeight) {
		Ensure::True(SDL_Init(SDL_INIT_EVERYTHING) == 0, "SDL failed to initialize.");

		mHandle = SDL_CreateWindow(
			mTitle,
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			mWidth,
			mHeight,
			SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI
			);

		Ensure::NotNull(mHandle, "SDL window failed to create.");
		mIsOpen = true;
		Log::Trace("Window created with Title: {} Width: {} Height: {}", mTitle, mWidth, mHeight);
	}

	WndCtx::~WndCtx() noexcept {
		SDL_DestroyWindow(As<SDL_Window>());
		SDL_Quit();
	}

	auto WndCtx::Update() noexcept -> b8 {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT:
					mIsOpen = false;
					break;
				case SDL_WINDOWEVENT:
					switch (event.window.event) {
						case SDL_WINDOWEVENT_CLOSE:
							mIsOpen = false;
							break;
						case SDL_WINDOWEVENT_RESIZED:
							mWidth = static_cast<u16>(event.window.data1);
							mHeight    = static_cast<u16>(event.window.data2);
							mIsResized = true;
							break;
						case SDL_WINDOWEVENT_MINIMIZED:
							mIsMinimized = true;
							break;
						case SDL_WINDOWEVENT_MAXIMIZED:
							mIsMaximized = true;
							break;
						case SDL_WINDOWEVENT_RESTORED:
							mIsMinimized = false;
							mIsMaximized = false;
							break;
						case SDL_WINDOWEVENT_FOCUS_GAINED:
							mIsFocused = true;
							break;
						case SDL_WINDOWEVENT_FOCUS_LOST:
							mIsFocused = false;
							break;
						default: break;
					}
					break;
				default: break;
			}
		}
		return mIsOpen;
	}
} // Kryogenic
