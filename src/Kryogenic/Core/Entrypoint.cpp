#include "Kryogenic/Core/AppCtx.hpp"
#include "Kryogenic/Foundation/Log.hpp"

auto main(i32 const pArgc, char* pArgv[]) -> i32 {
	(void)pArgc;
	(void)pArgv;

	using namespace Kryogenic;

	auto const appCtx = std::make_unique<AppCtx>();
	appCtx->Execute();

	return EXIT_SUCCESS;
}
