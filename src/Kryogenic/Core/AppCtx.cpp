#include "AppCtx.hpp"

#include "Kryogenic/Foundation/Ensure.hpp"

namespace Kryogenic {
	AppCtx::AppCtx() noexcept {
		Ensure::Null(sInstance, "AppCtx instance already exists.");
		sInstance = this;

		mWndCtx = std::make_unique<WndCtx>(WndCtx::Desc{});
	}

	auto AppCtx::Instance() noexcept -> AppCtx& {
		Ensure::NotNull(sInstance, "AppCtx instance does not exist.");
		return *sInstance;
	}

	auto AppCtx::Execute() noexcept -> void {
		mIsRunning = true;
		while (mIsRunning) {
			mIsRunning = mWndCtx->Update();
		}
	}

	auto AppCtx::Terminate() noexcept -> void {
		mIsRunning = false;
	}
} // Kryogenic
